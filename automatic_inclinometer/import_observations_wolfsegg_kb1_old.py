""" import firebird, export to postgresql """
#!/usr/bin/python# -*- coding: utf-8 -*-

from typing import List
import uuid
from sqlalchemy.orm import session
from sqlalchemy import desc, asc
from db.fb_models import (create_session, FbObservation, Catena)
from db.models import (create_pg_session, Dataset, Observation, Procedure, Phenomenon, Platform)

def main():
    """
    Main function.
    """

    # parameter:
    # sensor id in firebird db:
    # sensor_id = 1
    # # name of project area in firebird db
    # feature_of_interest = 'TAC003-020-0517' # Wolfsegg KB1
    # # sensor name in postgis db
    # sensor = 'wolfsegg_kb1_1'
    # platform = 'wolfsegg'

    sensor_id = 0
    # name of project area in firebird db
    feature_of_interest = 'TAC003-020-0517' # Wolfsegg KB1
    # sensor name in postgis db
    sensor = 'wolfsegg_kb1_0'
    platform = 'wolfsegg_kb1_inclinometer'

    firebird_session: session = create_session()
    # db_observation = session.query(Observation) \
    # .filter_by(name='John Snow').first()
    query = firebird_session.query(FbObservation).join(FbObservation.catena) \
        .filter(FbObservation.sensore == sensor_id) \
        .filter(Catena.name == feature_of_interest)
    # feature_of_interest = query.statement.compile(dialect=firebird.dialect())
    firebird_observations: List[FbObservation] = query.all()
    firebird_session.close()

    pg_session: session = create_pg_session()
    # pg_datasets: List[Dataset] = pg_query.all()
    pg_query = pg_session.query(Dataset) \
        .join(Procedure) \
        .join(Phenomenon) \
        .filter(Procedure.sta_identifier == sensor.lower())
        # .join(Platform).all() \


    # roll_dataset = [x for x in pg_datasets if x.phenomenon.sta_identifier  == "Roll"]
    roll_dataset = pg_query.filter(Phenomenon.sta_identifier == "Roll").first()
    roll_dataset.is_published = 1
    roll_dataset.is_hidden = 0
    roll_dataset.dataset_type = "timeseries"
    roll_dataset.observation_type = "simple"
    roll_dataset.value_type = "quantity"
    slope_dataset = pg_query.filter(
        Phenomenon.sta_identifier == "Slope").first()
    slope_dataset.is_published = 1
    slope_dataset.is_hidden = 0
    slope_dataset.dataset_type = "timeseries"
    slope_dataset.observation_type = "simple"
    slope_dataset.value_type = "quantity"
    temperature_dataset = pg_query.filter(
        Phenomenon.sta_identifier == "InSystemTemperature").first()
    temperature_dataset.is_published = 1
    temperature_dataset.is_hidden = 0
    temperature_dataset.dataset_type = "timeseries"
    temperature_dataset.observation_type = "simple"
    temperature_dataset.value_type = "quantity"
    pg_session.commit()

    # max_id = pg_session.query(func.max(Observation.id)).scalar()
    for fb_observation in firebird_observations:
        # print(fb_observation.catena.name)
        if(fb_observation.roll is not None and roll_dataset is not None):
            # max_id = max_id + 1
            pg_roll_observation = Observation(
                # id=max_id,
                value_type='quantity',
                sampling_time_start=fb_observation.result_time,
                sampling_time_end=fb_observation.result_time,
                result_time=fb_observation.result_time,
                sta_identifier=str(uuid.uuid4()),
                value_quantity=fb_observation.roll
            )
            roll_dataset.observations.append(pg_roll_observation)
        if(fb_observation.pitch is not None and slope_dataset is not None):
            # max_id = max_id + 1
            pg_slope_observation = Observation(
                # id=max_id,
                value_type='quantity',
                sampling_time_start=fb_observation.result_time,
                sampling_time_end=fb_observation.result_time,
                result_time=fb_observation.result_time,
                sta_identifier=str(uuid.uuid4()),
                value_quantity=fb_observation.pitch
            )
            slope_dataset.observations.append(pg_slope_observation)
        if(fb_observation.temperature is not None and temperature_dataset is not None):
            # max_id = max_id + 1
            pg_temperature_observation = Observation(
                # id=max_id,
                value_type='quantity',
                sampling_time_start=fb_observation.result_time,
                sampling_time_end=fb_observation.result_time,
                result_time=fb_observation.result_time,
                sta_identifier=str(uuid.uuid4()),
                value_quantity=fb_observation.temperature
            )
            temperature_dataset.observations.append(pg_temperature_observation)
    # commit observations:
    pg_session.commit()

    last_roll_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == roll_dataset.id) \
        .order_by(desc('sampling_time_start')) \
        .first()
    if last_roll_observation is not None:
        roll_dataset.last_time = last_roll_observation.sampling_time_start
        roll_dataset.last_value = last_roll_observation.value_quantity
        roll_dataset.fk_last_observation_id = last_roll_observation.id

    last_slope_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == slope_dataset.id) \
        .order_by(desc('sampling_time_start')) \
        .first()
    if last_slope_observation is not None:
        slope_dataset.last_time = last_slope_observation.sampling_time_start
        slope_dataset.last_value = last_slope_observation.value_quantity
        slope_dataset.fk_last_observation_id = last_slope_observation.id

    last_temperature_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == temperature_dataset.id) \
        .order_by(desc('sampling_time_start')) \
        .first()
    if last_temperature_observation is not None:
        temperature_dataset.last_time = last_temperature_observation.sampling_time_start
        temperature_dataset.last_value = last_temperature_observation.value_quantity
        temperature_dataset.fk_last_observation_id = last_temperature_observation.id

    first_roll_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == roll_dataset.id) \
        .order_by(asc('sampling_time_start')) \
        .first()
    if first_roll_observation is not None:
        roll_dataset.first_time = first_roll_observation.sampling_time_start
        roll_dataset.first_value = first_roll_observation.value_quantity
        roll_dataset.fk_first_observation_id = first_roll_observation.id

    first_slope_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == slope_dataset.id) \
        .order_by(asc('sampling_time_start')) \
        .first()
    if first_slope_observation is not None:
        slope_dataset.first_time = first_slope_observation.sampling_time_start
        slope_dataset.first_value = first_slope_observation.value_quantity
        slope_dataset.fk_first_observation_id = first_slope_observation.id

    first_temperature_observation = pg_session.query(Observation) \
        .filter(Observation.fk_dataset_id == temperature_dataset.id) \
        .order_by(asc('sampling_time_start')) \
        .first()
    if first_temperature_observation is not None:
        temperature_dataset.first_time = first_temperature_observation.sampling_time_start
        temperature_dataset.first_value = first_temperature_observation.value_quantity
        temperature_dataset.fk_first_observation_id = first_temperature_observation.id

    platform_exists = pg_session.query(Platform.id).filter_by(
        name=platform.lower()).scalar() is not None
    if not platform_exists:
        sensor_platform = Platform()
        # max_id = pg_session.query(func.max(Platform.id)).scalar()
        # sensor_platform.id = max_id + 1
        sensor_platform.sta_identifier = platform.lower()
        sensor_platform.identifier = platform.lower()
        sensor_platform.name = platform.lower()
        slope_dataset.platform = sensor_platform
        roll_dataset.platform = sensor_platform
        temperature_dataset.platform = sensor_platform
    else:
        sensor_platform = pg_session.query(Platform.id) \
            .filter(Platform.name == platform.lower()) \
            .first()
        slope_dataset.fk_platform_id = sensor_platform.id
        roll_dataset.fk_platform_id = sensor_platform.id
        temperature_dataset.fk_platform_id = sensor_platform.id

    # commit dataset changes:
    pg_session.commit()
    pg_session.close()


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    main()
