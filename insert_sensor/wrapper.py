# -*- coding: utf-8 -*-
"""This module does blah blah."""


class Offering:
    """ offering class """
    def __init__(self, rooturl, offering_name, offering_label):
        self.url = rooturl
        # self.identifier = offeringId
        self.name = str(offering_name)
        self.label = offering_label
        #self.fullId = str(rooturl) + str(offeringId)


class Procedure:
    """ procedure class """
    def __init__(self, procedure_id, procedure_name):
        self.id = procedure_id
        self.name = procedure_name


# Feature of Interest
class FoI:
    """ foi class """
    def __init__(self, xy_unit, z_unit, cords, feature_id, feature_name):
        '''
        :param xy_unit: unit for X,Y coordinates. Eg. degrees, meters, etc.
        :param z_unit: unit for z, usually height in meters.
        :param cords: a tuple like (X, Y, Z)
        :param feature_id:  id for the feature of interest
        '''
        self.x = cords[0]
        self.y = cords[1]
        self.z = cords[2]
        self.Vunit = str(z_unit)  # unit of the vertical dimension
        self.Hunit = str(xy_unit)  # unit of the horizontal dimensions
        self.fid = feature_id  # ID of the feature
        self.name = feature_name


class SensorType:
    """ sensor type class """

    # In a SOS sensors can be:
    # (a) In-situ ('on the spot') or (b) remote (e.g. satellites, airborne)
    # (1) stationary (with fixed location) or mobile (in movement).
    # Classification used in this class.
    # TODO: extend class to consider all types.

    om_types = {"m": "OM_Measurement", "co": "OM_CategoryObservation", "cto": "OM_CountObservation",
                "to": "OM_TextObservation", "go": "OM_GeometryObservation", "tho": "OM_TruthObservation",
                "xo": "OM_ComplexObservation"}

    def __init__(self, type_):  # type refers to the description of phenomena observed,
        #  and the mobility of the the sensor.
        # TODO: work on an ontology to deal with different phenomena names
        if type_ == "light":  # LIGHT
            self.pattern = {"name": "light", "type": 'fixed', "attributes": [
                ("Luminosity", "m"), ("Battery level", "m"), ("Temperature", "m")]}
        elif type_ == "bus":  # BUS
            self.pattern = {"name": "BUS", "type": 'mobile', "attributes": [("Speed", "m"), ("Course", "m"), ("Odometer", "m"), ("CO", "m"), (
                "Particles", "m"), ("Ozone N02", "m"), ("N02", "m"), ("Temperature", "m"), ("Humidity", "m")]}  # ("Location","go")]}
        elif type_ == "env_station":  # ENV_STATION
            self.pattern = {"name": "env_station", "type": 'fixed', "attributes": [("Battery level", "m"), ("Temperature", "m"), ("Relative humidity", "m"), ("Soil Moisture", "m"), (
                "Solar Radiation", "m"), ("Rainfall", "m"), ("Wind_Speed", "m"), ("Wind_Direction", "m"), ("Radiation_PAR", "m"), ("Atmospheric Pressure", "m")]}
        elif type_ == "irrigation":  # IRRIGATION
            self.pattern = {"name": "irrigation", "type": 'fixed', "attributes": [("Battery level", "m"), (
                "Temperature", "m"), ("Relative humidity", "m"), ("Soil Moisture", "m"), ("Soil Temperature", "m")]}
        elif type_ == "agriculture":  # AGRICULTURE
            self.pattern = {"name": "agriculture", "type": 'fixed', "attributes": [
                ("Battery level", "m"), ("Temperature", "m"), ("Relative humidity", "m")]}
        elif type_ == "inclinometer":  # INCLINOMETER
            self.pattern = {"name": "inclinometer", "type": 'fixed', "attributes": [
                ("Slope", "m"), ("Roll", "m"), ("Temperature", "m")]}
        elif type_ == "camera":  # Camera
            self.pattern = {"name": "camera", "type": 'fixed', "attributes": [
                ("Image", "to")]}
        elif type_ == "piezometer":  # PIEZOMETER
            self.pattern = {"name": "piezometer", "type": 'fixed', "attributes": [
                ("Elevation", "m")]}
        elif type_ == "tachymeter":  # TACHYMETER
            self.pattern = {"name": "tachymeter", "type": 'mobile', "attributes": [
                ("TachymeterLocation", "go")]}
        elif type_ == "noise":  # NOISE
            self.pattern = {"name": "noise", "type": 'fixed', "attributes": [
                ("Battery level", "m"), ("Noise", "m")]}
        elif type_ == "vehicle_counter":  # VEHICLE_COUNTER
            self.pattern = {"name": "vehicle_counter", "type": 'fixed', "attributes": [
                ("Occupancy", "m"), (" Count", "cto")]}
        elif type_ == "vehicle_speed":  # VEHICLE_SPEED
            self.pattern = {"name": "vehicle_speed", "type": 'fixed', "attributes": [
                ("Occupancy", "m"), (" Count", "cto"), (" Average Speed",  "m"), (" Median Speed",  "m")]}
        elif type_ == 'temp':  # TEMP
            self.pattern = {"name": "temp", "type": 'fixed', "attributes": [
                ("Battery level", "m"), ("Temperature", "m")]}
        elif type_ == 'outdoor':  # Low EMF, measuring 'electrosmog'
            # EFM-project, http://lexnet-project.eu/
            self.pattern = {"name": "outdoor", "type": 'fixed', "attributes": [(" EField (900 Mhz)", "m"), (
                " EField (1800 Mhz)", "m"), (" EField (2100 Mhz)", "m"), (" EField (2400 Mhz)", "m")]}
        elif type_ == 'waste':  # WASTE COLLECTOR (Truck)
            self.pattern = {"name": "waste", "type": "fixed", "attributes": [("temperature", "m"), (
                "humidity", "m"), ("particles", "m"), ("CO", "m"), ("NO2", "m"), ("O3", "m"), ("Location", "go")]}
        elif type_ == 'air':  # AIR, Not currently reporting
            self.pattern = {"name": "air", "type": "fixed"}
        else:
            print("Sensor type is not defined")
