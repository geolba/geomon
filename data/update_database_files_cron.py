'''
download daily database files
Python version: 3.10
'''
import os
import requests
# import uuid
# import json
# from dotenv import load_dotenv, find_dotenv


def main():
    """
    Main function.
    """
    ######### download firebird db ####################################################

    # efine the remote file to retrieve
    firebird_url = 'https://rhea.geologie.ac.at/index.php/s/Uqkvso33Vp7nUTn/download'
    # Define the local filename to save data
    local_firebird_file = '/home/administrator/databases/67.Fdb'
    # Make http request for remote file data
    data = requests.get(firebird_url)
    # Save file data to local copy
    with open(local_firebird_file, 'wb')as file:
        file.write(data.content)
        os.chmod(local_firebird_file, 0o777)
    print(f"firebird db file {local_firebird_file} has been succesfully updated!")

     ######### downlod Voegelsber data.txt ####################################################
    voegelsberg_data_url = 'https://rhea.geologie.ac.at/index.php/s/esMBbekhrjIebkH/download'
    # Define the local filename to save data
    local_voegelsberg_file = '/home/administrator/databases/data.txt'
    # Make http request for remote file data
    data = requests.get(voegelsberg_data_url)
    # Save file data to local copy
    with open(local_voegelsberg_file, 'wb')as file:
        file.write(data.content)
    print(f"voegelsberg data file {local_voegelsberg_file} has been succesfully updated!")

    # add cron job:
    # sudo crontab -e
    # add the following line:
    # 0 * * * * /etc/geomon/.venv/bin/python /etc/geomon/data/update_database_files_cron.py


if __name__ == "__main__":
    main()
