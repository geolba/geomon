# -*- coding: utf-8 -*-
"""This module does blah blah."""

import requests
from transactional import insert_sensor
from wrapper import (Offering, FoI, Procedure, SensorType)
# import json


class Sos():
    """
    A class to represent a sos service.
    ...

    Attributes
    ----------
    sosurl : str
        first name of the person
    token : str
        token to access soso service
    """

    def __init__(self, url, token=''):
        self.sosurl = str(url)  # url to access the SOS
        self.token = str(token)  # security token, optional
        # Test if URL exists
        try:
            test = requests.get(self.sosurl)
            test.raise_for_status()
        except requests.HTTPError:
            print("The URL is not valid")


def main():
    """
    main function
    """
    sos_url = 'https://geomon.geologie.ac.at/52n-sos-webapp/service'

    ######################## Sibratsgfall
    # offering = Offering(
    #     "https://geomon.geologie.ac.at/52n-sos-webapp/api/offerings/",
    #     "sibratsgfall_3",
    #     "Inklinometer 3, Sibratsgfaell Sensor"
    # )
    # procedure = Procedure( "sibratsgfall_3","sibratsgfall-3")
    # foi = FoI("degree", "m", (47.4279288, 10.0360888, 0.0),
    #           "sibratsgfall", "Sibratsgfall Beobachtung der Bodenbewegungen Test")

     ####################### Gschliefgraben Glasfaser
    offering = Offering(
        "https://geomon.geologie.ac.at/52n-sos-webapp/api/offerings/",
        "inclino1_02",
        "Inklinometer inclino1_02, Gschliefgraben Glasfaser"
    )
    procedure = Procedure( "inclino1_02","inclino1_02")

    foi = FoI("degree", "m", (47.910849, 13.774966, 0.0),
              "FBGuard23", "Glasfaser Untersuchungen am Gschliefgraben (Gmunden)")

    ######################## Gschliefgraben
    # offering = Offering(
    #     "https://geomon.geologie.ac.at/52n-sos-webapp/api/offerings/",
    #     "gschliefgraben_2",
    #     "Inklinometer 2, Gschliefgraben Sensor"
    # )
    # procedure = Procedure( "gschliefgraben_2","gschliefgraben-2")

    # foi = FoI("degree", "m", (47.8845629, 13.8199351, 0.0),
    #           "GSA01A-033-0909", "Geophysikalische Untersuchungen am Gschliefgraben (Gmunden)")

    ######################## Laakirchen
    # offering = Offering(
    #     "https://geomon.geologie.ac.at/52n-sos-webapp/api/offerings/",
    #     "laakirchen_10",
    #     "Inklinometer 10, Laakirchen Sensor"
    # )
    # procedure = Procedure( "laakirchen_10","laakirchen-10")

    # foi = FoI("degree", "m", (47.9789118, 13.8141457, 0.0),
    #           "GSA02B-007-0911", "Massenbewegung Laakirchen")


    sensor_type = SensorType("inclinometer")
    post_data = insert_sensor(offering, procedure,  foi, sensor_type)
    print(post_data)
    headers = {'Accept': 'application/json'}
    request = requests.post(sos_url, headers=headers, json=post_data)
    print(request.text)

    # {
    # "request" : "InsertSensor",
    # "version" : "2.0.0",
    # "service" : "SOS",
    # "assignedProcedure" : "inclino1_14",
    # "assignedOffering" : "inclino1_14"
    # }


if __name__ == '__main__':
    main()
