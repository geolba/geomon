# -*- coding: utf-8 -*-
"""
Function for the SOS Transactional profile.
This set of function format requests to publish and handle data in a SOS using a RESTful API.
Requests need to be passed as the body of a HTTP request to the SOS server.
When more than one syntax is allowed, requests as passed using XML version 2.0
Author: Arno Kaimbacher
Created: 12-07-2021
"""


def insert_sensor(offering, procedure,  foi, sensor_type):
    """
    Prepares the body of a InsertSensor request for JSON biding.
    :param offering: an instance of class Offering.Type object.
    :param Procedure: instance of class Procedure. type object.
    :param foi: feature of interest. Instance of FoI
    :param sensor_type: SensorType object
    :return: valid body for an InsertSensor request.
    """

    # shortName = offering.name  # string
    # longName = 'Sibratsgfall test'  # string

    # Offering values
    off_name = '\"' + str(offering.name) + '\"'  # Offering name, double quoted
    offering_name = offering.name
    offering_label = offering.label
    # offID = offering.fullId  # URL format of full id

    # featureName = featureID = cordX = cordY = height = h_unit = z_unit = coordinates = ""
    if foi is not None:  # check if feature of interest should be declare
        # feature_id = 'https://geomon.geologie.ac.at/52n-sos-webapp/api/features/' + \
        #     str(foi.fid)  # URL format
        cord_x = str(foi.x)  # longitude degrees, float
        cord_y = str(foi.y)  # latitude degrees, float
        coordinates = cord_x + " " + cord_y
        height = str(foi.z)		# altitude in meters, float
        # h_unit = foi.Hunit  # units for horizontal coordinates
        # z_unit = foi.Vunit  # units for altitude
        feature_id = foi.fid # "feature location"
        feature_name = foi.name  # "feature location"
    else:
        pass

    procedure_name = procedure.name
    procedure_identifier = procedure.id  # URL,
    obs_types = []
    output_list = ''  # output list element for describe procedure
    properties_list = []
    for a in sensor_type.pattern["attributes"]:
        obs_prop_name = '\"' + a[0] + '\"'  # attribute name
        # print(obs_prop_name)
        unit_name = sensor_type.om_types[a[1]]  # om type
        # magnitud = a  # ??

        obs_name = obs_prop_name.replace('\"', '')
        obs_name = "".join(obs_name.split())  # observable property name
        output = '<sml:output name=' + obs_prop_name + '><swe:Quantity definition=' + \
            '\"' + (obs_name) + '\"' + \
            '></swe:Quantity></sml:output>'
        output_list = output_list + output
        # add property identifier to the list.
        properties_list.append(obs_name)
        #  prepare list of measurement types
        # A sensor can not registry duplicated sensor types.
        this_type = "http://www.opengis.net/def/observationType/OGC-OM/2.0/"+unit_name
        if this_type not in obs_types:  # when new type appears
            obs_types.append(this_type)
        else:
            continue

    # Unit of measurement:
    unit_name = '\"' + procedure.name + '\"'  # double quoted string
    # unit = omType # one of the MO measurement types

    body = {
        "request": "InsertSensor",
        "service": "SOS",
        "version": "2.0.0",
        "procedureDescriptionFormat": "http://www.opengis.net/sensorml/2.0",
        "procedureDescription": f'<sml:PhysicalSystem gml:id={off_name} xmlns:swes=\"http://www.opengis.net/swes/2.0\" xmlns:sos=\"http://www.opengis.net/sos/2.0\" xmlns:swe=\"http://www.opengis.net/swe/2.0\" xmlns:sml=\"http://www.opengis.net/sensorml/2.0\" xmlns:gml=\"http://www.opengis.net/gml/3.2\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\"><gml:identifier codeSpace=\"uniqueID\">{procedure_identifier}</gml:identifier><sml:identification><sml:IdentifierList><sml:identifier><sml:Term definition=\"urn:ogc:def:identifier:OGC:1.0:shortName\"><sml:label>shortName</sml:label><sml:value>{procedure_name}</sml:value></sml:Term></sml:identifier></sml:IdentifierList></sml:identification><sml:capabilities name=\"offerings\"><sml:CapabilityList><sml:capability name=\"offeringID\"><swe:Text definition=\"urn:ogc:def:identifier:OGC:offeringID\"><swe:label>{offering_label}</swe:label><swe:value>{offering_name}</swe:value></swe:Text></sml:capability></sml:CapabilityList></sml:capabilities><sml:featuresOfInterest><sml:FeatureList definition=\"http://www.opengis.net/def/featureOfInterest/identifier\"><swe:label>featuresOfInterest</swe:label><sml:feature><sams:SF_SpatialSamplingFeature xmlns:sams=\"http://www.opengis.net/samplingSpatial/2.0\" gml:id=\"ssf_b3a826dd44012201b01323232323041f7a92e0cc47260eb9888f6a4e9f747\"><gml:identifier codeSpace=\"http://www.opengis.net/def/nil/OGC/0/unknown\">{feature_id}</gml:identifier><gml:name codeSpace=\"http://www.opengis.net/def/nil/OGC/0/unknown\">{feature_name}</gml:name><sf:type xmlns:sf=\"http://www.opengis.net/sampling/2.0\" xlink:href=\"http://www.opengis.net/def/samplingFeatureType/OGC-OM/2.0/SF_SamplingPoint\"/><sf:sampledFeature xmlns:sf=\"http://www.opengis.net/sampling/2.0\" xlink:href=\"http://www.opengis.net/def/nil/OGC/0/unknown\"/><sams:shape><ns:Point xmlns:ns=\"http://www.opengis.net/gml/3.2\" ns:id=\"Point_ssf_b3a826dd44012201b013c90c51da28c041f7a92e0cc47260eb9888f6a4e9f747\"><ns:pos srsName=\"http://www.opengis.net/def/crs/EPSG/0/4326\">{coordinates}</ns:pos></ns:Point></sams:shape></sams:SF_SpatialSamplingFeature></sml:feature></sml:FeatureList></sml:featuresOfInterest><sml:outputs><sml:OutputList><sml:output name=\"Slope\"><swe:Quantity definition=\"Slope\"><swe:label>Slope</swe:label><swe:uom code=\"deg\"/></swe:Quantity></sml:output></sml:OutputList></sml:outputs><sml:position><swe:Vector referenceFrame=\"urn:ogc:def:crs:EPSG::4326\"><swe:coordinate name=\"easting\"><swe:Quantity axisID=\"x\"><swe:uom code=\"degree\" /><swe:value>{cord_x}</swe:value></swe:Quantity></swe:coordinate><swe:coordinate name=\"northing\"><swe:Quantity axisID=\"y\"><swe:uom code=\"degree\" /><swe:value>{cord_y}</swe:value></swe:Quantity></swe:coordinate><swe:coordinate name=\"altitude\"><swe:Quantity axisID=\"z\"><swe:uom code=\"m\" /><swe:value>{height}</swe:value></swe:Quantity></swe:coordinate></swe:Vector></sml:position></sml:PhysicalSystem>',
        "observableProperty": [
            "Slope",
            "Roll",
            "InSystemTemperature"
        ],
        "observationType": [
            "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement"
        ],
        "featureOfInterestType": "http://www.opengis.net/def/samplingFeatureType/OGC-OM/2.0/SF_SamplingPoint"
    }
    return body
